package me.alexbanper.projects.dj;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.plugin.java.JavaPlugin;

import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;

public class Main extends JavaPlugin implements Listener
{
	public void onEnable()
	{
		loadConfiguration();
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getLogger().info("Double Jump V 1.8 Enabled!");
		Bukkit.getLogger().info("Created by: AlexBanPer");
	}

	public void onDisable()
	{
		Bukkit.getLogger().info("Double Jump Disabled!");
	}

	public void loadConfiguration()
	{
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	@EventHandler
	public void onPlayerJump(PlayerToggleFlightEvent e)
	{
		Player player = e.getPlayer();

		if (player.getGameMode() == GameMode.CREATIVE)
		{
			return;
		}
		
		for (String world : getConfig().getStringList("Config.Enabled_Worlds")) {
			if (player.getWorld().getName().equalsIgnoreCase(world))
			{
				e.setCancelled(true);
				player.setAllowFlight(false);
				player.setFlying(false);
				if (player.hasPermission("doublejump.use"))
				{
					player.setVelocity(player.getLocation().getDirection().multiply(getConfig().getInt("Config.Force")).setY(getConfig().getInt("Config.Up")));
					player.playSound(player.getLocation(), Sound.valueOf(getConfig().getString("Config.Sound")), 10.0F, 7.0F);
					return;
				}
				if(player.hasPermission("doublejump.vipuse") || !player.isOp())
				{
					player.setVelocity(player.getLocation().getDirection().multiply(getConfig().getInt("Config.VIP.Force", 1)).setY(getConfig().getInt("Config.VIP.Up", 1)));
					if(getConfig().getBoolean("Config.VIP.SoundToAll")  == true)
					{
						for(Player online : Bukkit.getOnlinePlayers())
						{
							online.playSound(player.getLocation(), Sound.valueOf(getConfig().getString("Config.VIP.Sound")), 10.0F, 7.0F);
						}
					}else{
						player.playSound(player.getLocation(), Sound.valueOf(getConfig().getString("Config.VIP.Sound")), 10.0F, 7.0F);
					}
					return;
				}
			}
			else
			{
				player.setAllowFlight(false);
				player.setFlying(false);
			}
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		Player player = e.getPlayer();
		PacketPlayOutWorldParticles joinParticles = new PacketPlayOutWorldParticles(
				EnumParticle.DRAGON_BREATH,
				true,
				(float) player.getLocation().getX(),
				(float) player.getLocation().getY(),
				(float) player.getLocation().getZ(), 
				(float) 0,
				(float) 0,
				(float) 0,
				(float) 0.1,
				500
				);

		if(player.hasPermission("doublejump.vipuse"))
		{
			for(Player online : Bukkit.getOnlinePlayers())
			{
				((CraftPlayer)online).getHandle().playerConnection.sendPacket(joinParticles);
			}
			player.playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_HORSE_HURT,1,1);
		}
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e)
	{
		Player player = e.getPlayer();
		if ((player.getGameMode() != GameMode.CREATIVE) && 
				(player.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() != Material.AIR) && 
				(!player.isFlying())) {
			player.setAllowFlight(true);
		}
	}
}